from . models import Product, Order
from django import forms


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ('person_name', 'person_phone', 'message', 'order_product')
