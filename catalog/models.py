# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.contrib import admin
from django.utils import timezone


class Category(models.Model):
    ctg_name = models.CharField(max_length=250, verbose_name=u'Название категории')
    ctg_image = models.ImageField(upload_to='images', blank=True, verbose_name=u'Изображение категории')
    ctg_slug = models.SlugField(max_length=250, verbose_name=u'URL-ссылка',help_text=u'Сокращенный заголовок латиницей, используется для создания человекопонятного URL, должен быть уникальным')
    ctg_show = models.BooleanField(default=False, verbose_name=u'Показывать на сайте')
    ctg_sort = models.IntegerField(default=0, verbose_name=u'Сортировка')

    def __unicode__(self):
        return self.ctg_name

    class Meta:
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'


class Product(models.Model):
    prod_name = models.CharField(max_length=250, verbose_name=u'Наименование товара')
    prod_categ = models.ForeignKey(Category, verbose_name=u'Категория товара', null=True, blank=True)
    prod_image = models.ImageField(upload_to='images', blank=True, verbose_name=u'Изображение товара')
    prod_price = models.IntegerField(default=0, verbose_name=u'Цена (руб.)')
    prod_oldprice = models.IntegerField(default=0, verbose_name=u'Старая цена (руб.)')
    prod_weight = models.PositiveIntegerField(default=0, verbose_name=u'Вес (грамм)')
    prod_isnovelty = models.BooleanField(default=False, verbose_name=u'Новинка')
    prod_ishit = models.BooleanField(default=False, verbose_name=u'Хит')
    prod_show = models.BooleanField(default=False, verbose_name=u'Показывать на сайте')
    prod_descr = RichTextUploadingField(blank=True, default='')

    def __unicode__(self):
        return self.prod_name

    class Meta:
        verbose_name = u'Товар'
        verbose_name_plural = u'Товары'


class Order(models.Model):
    order_date = models.DateTimeField(default=timezone.now)
    order_product = models.ForeignKey(Product)
    person_name = models.CharField(max_length=250)
    person_phone = models.CharField(max_length=250)
    is_new = models.BooleanField(default=True)
    message = models.TextField(null=True, blank=True)

    def makeorder(self):
        self.save()

    def __unicode__(self):
        return self.order_date.strftime('%c')