from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^product/(?P<pk>[0-9]+)/$', views.CatalogView.as_view(page='product'), name='product'),
    url(r'^catalog$', views.CatalogView.as_view(page='catalog'), name='catalog'),
    url(r'^category/(?P<pk>[0-9]+)/$', views.CatalogView.as_view(page='category'), name='category'),
    url(r'^order/$', views.OrderView.as_view(), name='order_path'),
]
