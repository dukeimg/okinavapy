# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404
from .models import Category, Product, Order
from django.db.models import Q
from django.views.generic.base import TemplateView
from django.views.generic import CreateView
from .forms import OrderForm
from django.http import HttpResponseRedirect


class BaseView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(BaseView, self).get_context_data(**kwargs)
        context['full_menu'] = Category.objects.filter(ctg_show=1)
        return context


class CatalogView(BaseView):
    page = None

    def get_context_data(self, **kwargs):
        context = super(CatalogView, self).get_context_data(**kwargs)
        context['order_form'] = OrderForm
        if self.page == 'product':
            self.template_name = 'catalog/product-detail.html'
            context['prodInfo'] = get_object_or_404(Product, pk=kwargs.get('pk'))
        elif self.page == 'category':
            self.template_name = 'catalog/catalog-list.html'
            category = get_object_or_404(Category, pk=kwargs.get('pk'))
            context['ctlgItems'] = Product.objects.filter(prod_show=1, prod_categ_id=kwargs.get('pk'))
        elif self.page == 'catalog':
            self.template_name = 'catalog/full-menu.html'
        return context


class MainView(BaseView):
    template_name = 'catalog/catalog-hits.html'

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        context['ctlgItems'] = Product.objects.filter(prod_show=1).filter(Q(prod_ishit=1) | Q(prod_isnovelty=1))
        return context


class OrderView(CreateView):
    model = Order
    form_class = OrderForm
    template_name = 'catalog/catalog-list.html'

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER')
