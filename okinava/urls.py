from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from okinava import settings
from catalog.views import MainView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', MainView.as_view(), name='main'),
    url(r'', include('catalog.urls', namespace="catalog")),
    url(r'', include('pages.urls', namespace="pages")),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
