from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^delivery$', views.PageView.as_view(page='delivery'), name='delivery_page'),
    url(r'^contacts$', views.PageView.as_view(page='contacts'), name='contact_page'),
]
