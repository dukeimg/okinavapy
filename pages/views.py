from django.shortcuts import render
from catalog.views import BaseView

# def delivery_page(request):
#     return render(request, 'pages/delivery.html')
#
# def contacts_page(request):
#     return render(request, 'pages/contacts.html')


class PageView(BaseView):
    page = None
    def get_context_data(self, **kwargs):
        context = super(PageView, self).get_context_data(**kwargs)
        if self.page == 'delivery':
            self.template_name = 'pages/delivery.html'
        elif self.page == 'contacts':
            self.template_name = 'pages/contacts.html'
        return context