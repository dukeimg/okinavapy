Django==1.9
django-ckeditor==5.1.1
django-static-precompiler==1.5
easy-thumbnails==2.3
mysqlclient==1.3.9
Pillow==3.4.2