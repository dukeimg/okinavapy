$( document ).ready(function() {

	$('.mob-menu').click(function(e) {
		var menuList = $('ul.nav-menu');
		$(this).toggleClass('open');
		menuList.slideToggle();
	});


	$('.action-slider').owlCarousel({
	    loop:		true,
		items: 		1,
	    margin:		0,
	    nav:		false,
		dots: 		true
	});

	$('ul.filter li a').click(function(e) {
        e.preventDefault();
		if ($(this).attr('data-id')){
			var sub_id = $(this).attr('data-id');
		};
		$("ul.sub-filter").each(function(){
			$(this).removeClass('opened');
			if ($(this).attr('data-id') === sub_id){
				$(this).addClass('opened');
			};
		});
    });


	$('.nav-menu').dropit({
		action: 'mouseenter'
	});
	$('.set-price').dropit({
		action: 'click'
	});


	$('a[data-modal]').magnificPopup({
		type: 'ajax',
		modal: 'true'
	});

	$('.catalog-list .item-content').matchHeight();

	$('.product-slider').owlCarousel({
	    loop:		true,
		items: 		1,
	    margin:		0,
	    nav:		true,
		dots: 		false
	});


	$('.rest-thumbs').owlCarousel({
		items: 		3,
	    margin:		0,
	    nav:		true,
		dots: 		false
	});

	$('.rest-thumbs .t-item').click(function(event) {
        event.preventDefault();
		$('.t-item').each(function(){
			$(this).removeClass('active');
		});
		$(this).addClass('active');
		var path = $(this).data("slide");
		var parent = $(this).parents('.resuarants .col .content');
		parent.find(".big-photo a").attr('href', path);
		parent.find(".big-photo img").attr('src', path);
	});

	$('.big-photo').magnificPopup({
		delegate: 'a',
		type: 'image',
		gallery: {
			enabled: false,
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		}
	});


	$('.a-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		gallery: {
			enabled: true,
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		}
	});

	$('.cart-btn').magnificPopup({
		type: 'inline'
	});


	$('#personal-cabinet').easytabs({
		animate: false
	});


	$('.cabinet').magnificPopup({
		type: 'ajax',
		modal: 'true'
	})


	$('input:radio[name="delivery"]').change(function(){
		if ($(this).attr('id')=="makeDelivery") {
			$('.addrDeliv').show();
		}
		else {
			$('.addrDeliv').hide();
		}
	});


	$('.cart-prod-detail .del-item').click(function(event){
		event.preventDefault();
		$(this).parents('.cart-item').addClass('deleted');
		setTimeout(function(){
			$('.deleted').remove();
		}, 500);
	});


	[].slice.call(document.querySelectorAll('.input-content input, .input-content textarea')).forEach(function(inputEl) {
		if(inputEl.value.trim() !== '') {
			classie.add(inputEl.parentNode, 'focused');
		}
		inputEl.addEventListener('focus', onInputFocus);
		inputEl.addEventListener('blur', onInputBlur);
	});

	function onInputFocus(ev) {
		classie.add(ev.target.parentNode, 'focused');
	};
	function onInputBlur(ev) {
		if(ev.target.value.trim() === '') {
			classie.remove(ev.target.parentNode, 'focused');
		}
	};


    // $('.cart-btn').on('click', function(){
    //     var cart = $('a.cart');
    //     // var imgtodrag = $('.image-box img');
		// var imgtodrag =
		// $(this).parents('.item-content').find('.image-box img');
    //     if (imgtodrag) {
    //         var imgclone = imgtodrag.clone()
    //             .offset({
    //             top: imgtodrag.offset().top,
    //             left: imgtodrag.offset().left
    //         })
    //             .css({
    //             'opacity': '0.8',
    //                 'position': 'absolute',
    //                 'height': '250px',
    //                 'width': '250px',
    //                 'z-index': '10000'
    //         })
    //             .appendTo($('body'))
    //             .animate({
    //             'top': cart.offset().top + 10,
    //                 'left': cart.offset().left + 10,
    //                 'width': 75,
    //                 'height': 75
    //         }, 1000, 'easeInOutExpo');
    //         setTimeout(function () {
    //             $('a.cart i').effect("bounce", {times: 3}, 'slow');
    //         }, 1500);
    //         imgclone.animate({
    //             'width': 0,
    //                 'height': 0
    //         }, function () {
    //             $(this).detach()
    //         });
    //     }
    // });



});
